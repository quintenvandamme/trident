String error_1 = '[error_1] No kernel provided.';
String error_2 = '[error_2] Please give a valid command.';
String error_3 = '[error_3] Please give a valid kernel.';
String error_4 =
    '[error_4] %kernel not found on kernel.ubuntu.com try another one.';
String error_5 =
    '[error_5] wsl command can only be used to install wsl kernels on wsl2.';
String error_6 = '[error_6] Nvidia cards are unsupported by trident.';
String error_7 = '[error_7] File missing when building was complete.';
String error_8 =
    '[error_8] Run the command with only one - or run -help to list all the commands.';
String error_9 =
    '[error_9] Failed to fetch latest release: can\'t check for updates.';
